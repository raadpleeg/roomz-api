<?php

Route::group(['prefix' => '/files'], function () {
    Route::get('/', 'Files\UserController@index');
    Route::post('/', 'Files\UserController@store');
    Route::get('/{name}', 'Files\UserController@show');
    Route::put('/{name}', 'Files\UserController@update');
    Route::delete('/{name}', 'Files\UserController@destroy');

});