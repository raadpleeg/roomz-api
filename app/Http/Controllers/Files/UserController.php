<?php namespace App\Http\Controllers\Files;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class UserController extends Controller
{
    private $targetPath = "uploads/";
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    /**
     * @return \Illuminate\Http\JsonResponse of all the files in the uploads map.
     */
    public function index()
    {
        return response()->json($this->getGlob());
    }

    /** Creates a .txt file in the uploads map.
     * @return \Illuminate\Http\JsonResponse of the filename generated
     */
    public function store()
    {
        $content = $this->request->input('content');

        if(is_null($content))
        {
            $content = '';
        }

        do {
            $filename = $this->createFileName();
        } while (file_exists($this->targetPath . $filename . ".txt"));

        $file = fopen($this->targetPath . $filename . ".txt", "w");
        fwrite($file,$content);

        return response()->json(["Filename" => $filename, "content" => $content]);
    }

    /**
     * @param $name
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($name)
    {
        return response()->json(file_get_contents($this->targetPath.$name.".txt"));

    }

    /**
     * @param $name
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($name)
    {
        $newText =$this->request->input('text');
        if(!is_null($newText))
        {
            $file = fopen($this->targetPath.$name.".txt", "w");
            fwrite($file,$newText);
            return response()->json(true);
        }
        return response()->json("No input text");
    }

    public function destroy($name)
    {
        if(!is_null($name)) {
            if(file_exists($this->targetPath.$name.".txt"))
            {
                if (unlink($this->targetPath . $name . ".txt"))
                {
                    return response()->json(true);
                }

                return response()->json(["Status" => "500 Unable to delete file"]);
            }
            return response()->json(["Status" => "404 File not found"]);
        }
        return response()->json(["Error"=>"Name required"]);

    }

    /**
     * @return array of the files on the server
     */
    private function getGlob()
    {
        return glob("uploads/*");

    }

    /** Generates a random filename to use.
     * @return string randomly generated filename
     */
    private function createFileName()
    {
        $startString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $name = str_split($startString);
        $nameToSet = "";
        for ($i = 0; $i <= 9; $i++) {
            $nameToSet = $nameToSet . $name[random_int(0, 61)];
        }
        return $nameToSet;
    }

}